# Java-Auto-Updater
Java auto updater is a software to download the latest jar from ftp server and restart your application.

# Author
Peter, mcheung63@hotmail.com

# Usage
1) Add this code into your main(), replace the ftpHost, ftpUsername, ftpPassword. Create a directory call "software" in your ftp server, upload your latest jar to your ftp. In this example, your jar should be called "softwareNameXXX..jar"

```
JavaAutoUpdater.run(ftpHost, 21, ftpUsername, ftpPassword, false, false, "software", "softwareName", args);
```
2) Now all coding are done. Run your jar, and it will follow this flow to check and download the latest jar
		
![Flow chart](https://gitlab.com/mcheung63/Java-Auto-Updater/raw/master/image/flowchart.png)

3) Here is the dialog if it detected a latest jar on ftp and trying to download it
		
![Flow chart](https://gitlab.com/mcheung63/Java-Auto-Updater/raw/master/image/screen1.png)

![Flow chart](https://gitlab.com/mcheung63/Java-Auto-Updater/raw/master/image/screen2.png)
